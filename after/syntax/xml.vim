" ===============================================================
" Kitty
" Language: XML
" Description: Overrides for XML
" Author: Jared Reardon
" Last Change: 2022/01/14 17:40
" Forked Mike Hartington's OceanicNext colorscheme
" using colors (with minor modification) from the
" kitty terminal, credit to the original authors.
" ===============================================================

syn region xmlEndTag
      \ start=+</+
      \ end=+>+
      \ contains=xmlTagN
syn match xmlTagN
      \ contained +</\s*[-a-zA-Z0-9]\++hs=s+2
